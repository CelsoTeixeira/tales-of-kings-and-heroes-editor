using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Tales_of_Kings_and_Heroes_Editor.Editor
{
    public static class LoadFilesUtility
    {

        public static HistoryEvent LoadFile()
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            openDialog.InitialDirectory = DirectoryController.Path;

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader r = new StreamReader(openDialog.FileName))
                {
                    var jsonSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };

                    string tempJson = r.ReadToEnd();
                    
                    HistoryEvent temp = JsonConvert.DeserializeObject<HistoryEvent>(tempJson, jsonSettings);

                    return temp;

                }
            }

            return null;
        }
    }
}
