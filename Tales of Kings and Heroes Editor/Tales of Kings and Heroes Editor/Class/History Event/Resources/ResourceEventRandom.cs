using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{
    [Serializable]
    public class HistoryEventRandom : ResourceEvent
    {
        public float MinimunAmount { get; private set; }
        public float MaximunAmount { get; private set; }

        public HistoryEventRandom (string text, EventType type, RaceType race, EventResourceType resType, 
                                           EventAmountType amoType, float minA, float maxA)
                                            : base(text, type, race, resType, amoType)
        {
            this.MinimunAmount = minA;
            this.MaximunAmount = maxA;
        }        
    }
}
