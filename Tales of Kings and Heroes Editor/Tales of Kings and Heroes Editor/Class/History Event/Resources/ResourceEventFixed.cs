using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{
    [Serializable]
    public enum EventResourceType
    {
        Initial = 0,

        IncreaseFood = 1,
        DecreaseFood = 2,

        IncreaseWater = 3,
        DecreaseWater = 4,

        IncreaseWood = 5,
        DecreaseWood = 6,
    }

    [Serializable]
    public class ResourceEventFixed : ResourceEvent
    {
        public float Amount { get; private set; }
        
        public ResourceEventFixed(string text, EventType type, RaceType race, EventResourceType resType, EventAmountType amoType, float amount) 
                                            : base(text, type, race, resType, amoType)
        {
            this.Amount = amount;
        }        
    }
}
