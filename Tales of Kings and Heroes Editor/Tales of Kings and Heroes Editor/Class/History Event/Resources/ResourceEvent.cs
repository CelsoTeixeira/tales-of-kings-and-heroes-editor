using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{
    [Serializable]
    public class ResourceEvent : HistoryEvent
    {
        public EventResourceType ResourceType { get; set; }
        public EventAmountType AmountType { get; set; }

        public ResourceEvent(string text, EventType type, RaceType race, EventResourceType resType, EventAmountType amoType) : base(text, type, race)
        {
            this.ResourceType = resType;
            this.AmountType = amoType;
        }
    }
}
