using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class.History_Event.Gods
{
    [Serializable]
    public enum Gods
    {
        God1 = 1,
        God2 = 2,
    }

    [Serializable]
    public class HistoryEventGod : HistoryEvent
    {
        public Gods God { get; set; }
        
        public HistoryEventGod(string text, EventType type, RaceType race, Gods god) : base(text, type, race)
        {
            this.God = god;
        }

        public HistoryEventGod(string text, EventType type, RaceType race, string author, string name, Gods god) : base(text, type, race, author, name)
        {
            this.God = god;
        }
    }
}
