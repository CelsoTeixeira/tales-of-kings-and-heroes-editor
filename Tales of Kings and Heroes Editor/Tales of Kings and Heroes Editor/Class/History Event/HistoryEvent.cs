using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tales_of_Kings_and_Heroes_Editor
{
    [Serializable]
    public enum EventType
    {
        Initial = 0,        //This is the initial state, if we're here we don't do anything.

        Resource = 1,       //This will be EventResourceType enum for beter description.        
        Population = 2,     //This will be EventPopulationType enum for better description.

        Lore = 3,           //This is supposed to expand on the races and its historys.

        Gods = 4,           //This is supposed to be only events relational to Gods.

        Commun = 5,         //This is supposed to be commun events that all races can trigger.

    }


    //NOTE(Celso): If the value its Commun, the Event can be 
    //triggered by any race ingame.
    [Serializable]
    public enum RaceType
    {
        Commun = 0,

        Race1 = 1, 
        Race2 = 2, 
        Race3 = 3,
    }

    [Serializable]
    public enum EventsBuffs
    {
        None = 0,

        Buff1 = 1,
        Buff2 = 2,
        Buff3 = 3,
    }

    [Serializable]
    public enum EventsDebuffs
    {
        None = 0,

        Debuff1 = 1,
        Debuff2 = 2,
        Debuff3 = 3,
    }

    /// <summary>
    ///     This is the base for every event we will have in game.
    /// We can use this if we only want to display some massage or
    /// tell some history to the player.
    /// </summary>

    //NOTE(Celso): Right now we only have the possibilite to select one buff or 
    //one debuff and it need to have some kind of duration. It will be cool if we 
    //expand it and allow for more Buffs and Debuffs sinside the same Event. Also 
    //allow permanent effects.
    
    //NOTE(Celso): We could do some Events that add some type of Buff/Debuff and a 
    //different one to counter/delete the effect. It adds a nice type of mechanic.

    [Serializable]
    public class HistoryEvent
    {
        public string Author { get; set; }              //The author name.
        public string Name { get; set; }                //The history event name.

        public string HistoryText { get; set; }             //The actual text.
        public EventType HistoryType { get; set; }          //The History Event Type.
        
        public RaceType RaceType { get; set; }          //The Race this event will be.
        
        public EventsBuffs Buff { get; set; }           //The buff we will apply.
        public EventsDebuffs Debuff { get; set; }       //The debuff we will apply.

        public int EffectDuration { get; set; }         //Buff/Debuff duration. Based on ticks or seasons?

        public HistoryEvent() { }

        public HistoryEvent(string text, EventType type, RaceType race)
        {
            this.HistoryType = type;
            this.HistoryText = text;
            this.RaceType = race;
        }

        public HistoryEvent(string text, EventType type, RaceType race, string author, string name)
        {
            this.HistoryType = type;
            this.HistoryText = text;
            this.RaceType = race;
            this.Author = author;
            this.Name = name;
        }        
    }
}