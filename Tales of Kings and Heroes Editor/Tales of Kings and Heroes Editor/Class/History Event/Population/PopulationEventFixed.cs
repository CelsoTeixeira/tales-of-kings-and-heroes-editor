using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Tales_of_Kings_and_Heroes_Editor.Class
{

    /// <summary>
    ///     This is the base for the Population Event, we require
    /// an Population Type and some Amount of people who will leave
    /// or arrive depending on the Pop Type.    
    /// </summary>

    [Serializable]
    public class PopulationEventFixed : PopulationEvent
    {   
        public int Amount { get; set; }
        
        public PopulationEventFixed(string text, EventType type, RaceType race, EventPopulationType popType, EventAmountType amoType, int amount) : base(text, type, race, popType, amoType)
        {
            this.Amount = amount;
        }
    }
}
