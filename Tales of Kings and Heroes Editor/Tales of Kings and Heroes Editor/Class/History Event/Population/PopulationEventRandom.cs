using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{

    /// <summary>
    ///     This is the event to be used when we want to 
    /// have a random amount of people arriving or leaving.
    /// </summary>
    [Serializable]
    class PopulationEventRandom : PopulationEvent
    {
        public int MinimunAmount { get; private set; }
        public int MaximunAmount { get; private set; }
        
        public PopulationEventRandom(string text, EventType type, RaceType race, EventPopulationType popType, 
                                            EventAmountType amoType, int minimun, int maximun) : base(text, type, race, popType, amoType)
        {
            this.MinimunAmount = minimun;
            this.MaximunAmount = maximun;
        } 
    }
}
