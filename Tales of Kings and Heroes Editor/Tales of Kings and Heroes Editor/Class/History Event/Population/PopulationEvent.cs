using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{
    [Serializable]
    public enum EventPopulationType
    {
        Initial = 0,

        PopulationArriving = 1,
        PopulationLeaving = 2,
    }

    public enum EventAmountType
    {
        Fixed,
        Random,
        None
    }

    [Serializable]
    public class PopulationEvent : HistoryEvent
    {
        public EventPopulationType PopulationType { get; set; }
        public EventAmountType AmountType { get; set; }

        public PopulationEvent(string text, EventType type, RaceType race, EventPopulationType popType, EventAmountType amoType)
            : base(text, type, race)
        {            
            this.PopulationType = popType;
            this.AmountType = amoType;
        }
    }
}