using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor
{
    public static class DirectoryController
    {
        public static string Path { get; private set; }
        
        public static string HistoryPath { get; private set; }
        public static string HistoryResourcePath { get; private set; }
        public static string HistoryPopulationPath { get; private set; }
        public static string HistoryGodsPath { get; private set; }

        public static string HeroesPath { get; private set; }
        
        //NOTE(Celso): Creating the default folder setup.
        public static void SetupDefaultPath()
        {
            Path = @"C:\Users\Public\Documents\Tales of Kings and Heroes";
            Directory.CreateDirectory(@"C:\Users\Public\Documents\Tales of Kings and Heroes");

            HistoryPath = @"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History";
            Directory.CreateDirectory(@"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History");

            HistoryResourcePath = @"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History\Resources";
            Directory.CreateDirectory(@"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History\Resources");

            HistoryPopulationPath = @"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History\Population";
            Directory.CreateDirectory(@"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History\Population");

            HistoryGodsPath = @"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History\Gods";
            Directory.CreateDirectory(@"C:\Users\Public\Documents\Tales of Kings and Heroes\Game History\Gods");

            HeroesPath = @"C:\Users\Public\Documents\Tales of Kings and Heroes\Heroes";
            Directory.CreateDirectory(@"C:\Users\Public\Documents\Tales of Kings and Heroes\Heroes");
        }

        public static void ChangePath(string path)
        {
            if (!Directory.Exists(@path))
            {
                Path = @path;
                Directory.CreateDirectory(@path);
            }
        }

    }
}
