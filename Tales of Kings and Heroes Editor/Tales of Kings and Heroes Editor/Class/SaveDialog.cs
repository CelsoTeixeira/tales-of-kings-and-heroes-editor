using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{
    public static class SaveDialog
    {
        public static SaveFileDialog CreateNewSaveDialog(string directory)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();

            saveDialog.DefaultExt = ".json";            //Defaut extension
            saveDialog.InitialDirectory = @directory;   //Default directory from our directory controller
            saveDialog.FilterIndex = 2;
            saveDialog.RestoreDirectory = true;
         
               
            return saveDialog;
        }

        public static void ChangeSavePath(string path, SaveFileDialog saveFile)
        {
            saveFile.InitialDirectory = @path;
        }

        public static DialogResult SaveNewObject(Object saveObj, SaveFileDialog saveDialog)
        {
            switch (saveDialog.ShowDialog())
            {
                case DialogResult.OK:
                    JSonHelper.WriteFile(saveObj, saveDialog.FileName);
                    return DialogResult.OK;
                    
                case DialogResult.Cancel:
                    return DialogResult.Cancel;

                default:
                    return DialogResult.Cancel;
            }            
        }
    }
}
