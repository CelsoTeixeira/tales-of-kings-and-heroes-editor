using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{
    //NOTE(Celso): Is there a better way to pass Enum value to ComboBox and vice versa?

    /// <summary>
    /// This a helper class. This is supposed to receive a string
    /// and return the appropriate enum value. If we don't receive
    /// a valid string, we will pass the Initial value as default.
    /// Maybe we can have a better way to do this with the ComboBox, 
    /// but I need to understand better.
    /// 
    /// We eliminate all the naked strings from the program.
    /// </summary>
    public static class EnumAnalyzer
    {
        public static EventType EventAnalyzer(string enumString)
        {
            switch (enumString)
            {
                case EventsTypeHelper.ResourceType:
                    return EventType.Resource;
                    
                case EventsTypeHelper.PopulationType:
                    return EventType.Population;

                case EventsTypeHelper.GodsType:
                    return EventType.Gods;

                case EventsTypeHelper.LoreType:
                    return EventType.Lore;

                //NOTE(Celso): We could return Commun and not initial.
                default:
                    return EventType.Initial;
            }
        }
        
        public static EventResourceType EventResourceAnalyzer(string enumString)
        {
            switch (enumString)
            {
                case EventsTypeHelper.IncreaseWater:
                    return EventResourceType.IncreaseWater;
                    
                case EventsTypeHelper.DecreaseWater:
                    return EventResourceType.DecreaseWater;
                
                case EventsTypeHelper.IncreaseFood:
                    return EventResourceType.IncreaseFood;
                
                case EventsTypeHelper.DecreaseFood:
                    return EventResourceType.DecreaseFood;
                
                case EventsTypeHelper.IncreaseWood:
                    return EventResourceType.IncreaseWood;
                
                case EventsTypeHelper.DecreaseWood:
                    return EventResourceType.DecreaseWood;
                
                default:
                    return EventResourceType.Initial;
            }
        }

        public static EventPopulationType EventPopulationAnalyzer(string enumString)
        {
            switch (enumString)
            {
                case EventsTypeHelper.PopulationArriving:
                    return EventPopulationType.PopulationArriving;
                    
                case EventsTypeHelper.PopulationLeaving:
                    return EventPopulationType.PopulationLeaving;

                default:
                    return EventPopulationType.Initial;
            }
        }

        public static RaceType RaceTypeAnalyzer(string enumString)
        {
            switch (enumString)
            {
                case "Commun":
                    return RaceType.Commun;

                case "Race 1":
                    return RaceType.Race1;

                case "Race 2":
                    return RaceType.Race2;

                case "Race 3":
                    return RaceType.Race3;

                default:
                    return RaceType.Commun;

            }
        }

        public static EventsBuffs BuffTypeAnalizer(string enumString)
        {
            switch (enumString)
            {
                case EventsTypeHelper.NoBuff:
                    return EventsBuffs.None;

                case EventsTypeHelper.Buff1:
                    return EventsBuffs.Buff1;

                case EventsTypeHelper.Buff2:
                    return EventsBuffs.Buff2;

                case EventsTypeHelper.Buff3:
                    return EventsBuffs.Buff3;

                default:
                    return EventsBuffs.None;
            }
        }

        public static EventsDebuffs DebuffTypeAnalyzer(string enumString)
        {
            switch (enumString)
            {
                case EventsTypeHelper.NoDebuff:
                    return EventsDebuffs.None;

                case EventsTypeHelper.Debuff1:
                    return EventsDebuffs.Debuff1;

                case EventsTypeHelper.Debuff2:
                    return EventsDebuffs.Debuff2;

                case EventsTypeHelper.Debuff3:
                    return EventsDebuffs.Debuff3;

                default:
                    return EventsDebuffs.None;
            }            
        }        
    }
}