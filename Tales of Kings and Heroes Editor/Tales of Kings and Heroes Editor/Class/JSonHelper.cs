using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Tales_of_Kings_and_Heroes_Editor
{
    //This is a helper class to make JSon writing easier.
    public static class JSonHelper
    {

        public static void WriteFile(object myObj, string path)
        {
            var jsonSettings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Auto};

            string jsonSerializer = JsonConvert.SerializeObject(myObj, Formatting.Indented, jsonSettings);
            
            File.WriteAllText(@path, jsonSerializer, Encoding.UTF8);
        }
    }
}