using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor
{
    /// <summary>
    /// This is where we hold all the naked strings, every state we need to
    /// add we should add here too.
    /// Everything is getting added here, maybe there is a clever way than this 
    /// dumb holder I'm doing.
    /// 
    /// </summary>
    public static class EventsTypeHelper
    {
        public const string InitialState = "Initial";       //Initial state common for every first enum

        //Event types
        public const string ResourceType = "Resource";          
        public const string PopulationType = "Population";
        public const string GodsType = "Gods";
        public const string LoreType = "Lore";
        
        //Race
        //TODO: Add the right race names.
        public const string Commun = "Commun";
        public const string Race1 = "Race1";
        public const string Race2 = "Race2";
        
        //Resource types
        public const string IncreaseWater = "IncreaseWater";
        public const string DecreaseWater = "DecreaseWater";

        public const string IncreaseFood = "IncreaseFood";
        public const string DecreaseFood = "DecreaseFood";

        public const string IncreaseWood = "IncreaseWood";
        public const string DecreaseWood = "DecreaseWood";
        
        //Resource/Population Advanced Type
        public const string FixedAmount = "FixedAmount";
        public const string RandomAmount = "RandomAmount";
        public const string NoneAmount = "None";

        //Population types
        public const string PopulationArriving = "PopulationArriving";
        public const string PopulationLeaving = "PopulationLeaving";

        //Buffs
        public const string NoBuff = "NoBuff";
        public const string Buff1 = "Buff1";
        public const string Buff2 = "Buff2";
        public const string Buff3 = "Buff3";

        //Debuffs
        public const string NoDebuff = "NoDebuff";
        public const string Debuff1 = "Debuff1";
        public const string Debuff2 = "Debuff2";
        public const string Debuff3 = "Debuff3";
    }
}