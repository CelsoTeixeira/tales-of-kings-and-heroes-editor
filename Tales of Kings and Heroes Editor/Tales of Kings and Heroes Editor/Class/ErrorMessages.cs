using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tales_of_Kings_and_Heroes_Editor.Class
{
    public static class ErrorMessages
    {
        //Messages
        //JSon -> Generating structures
        //Success
        public const string Message_Success_GenerateResourcesData = "JSon structures for Resource data generated successfully.";
        public const string Message_Success_GeneratePopulationData = "JSon structures for Population data generated successfully.";
        public const string Message_Success_GenerateGodData = "JSon structures for gods data generated successfully.";
        
        //Save Fail
        public const string Message_Fail_CantSave = "Save cancelled by the user.";

        //Buffs Debuffs Messages
        public const string Message_Fail_BuffDebuffDuration = "The buff/debuff duration its 0.";




        //Errors text fails
        public const string Message_Fail_NoHistoryText = "There is no text for the history event, cant save the files.";
        public const string Message_Fail_NoAuthor = "No author was defined, we're not saving the file.";
        public const string Message_Fail_NoHistoryName = "No history event name was defined, we're not saving the file.";

        public const string Message_Fail_NoRaceDefinied = "No race was defined, we are definig it to Commun. If you need something different you should change it on the ComboBox.";

        public const string Message_FailedNoEventType = "No event type was defined.";
        public const string Message_FailedNoAdvancedType = "Advanced event type not defined.";
        public const string Message_FailedNoResourceType = "Event type based on resources was not deifined.";
        public const string Message_FailedNoPopulationType = "Event type based on population was not defined.";
    }
}