using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Tales_of_Kings_and_Heroes_Editor.Class;
using Tales_of_Kings_and_Heroes_Editor.Editor;

namespace Tales_of_Kings_and_Heroes_Editor
{
    public partial class CreationMenu : Form
    {
        public CreationMenu()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //NOTE(Celso): Better way to enable/disable?
            //Disable all stuff from the Advanced Panel

            ResourceGroup.Enabled = false;
            ResourceGroup.Visible = false;

            PopulationGroup.Enabled = false;
            PopulationGroup.Visible = false;

            //NOTE(Celso): We could reuse this both groups on the resource and population groups?
            //Resources groups
            FixedAmountGroup.Enabled = false;
            FixedAmountGroup.Visible = false;

            RandomAmountGroup.Enabled = false;
            RandomAmountGroup.Visible = false;

            //Population groups
            PopulationFixedAmount_Group.Enabled = false;
            PopulationFixedAmount_Group.Visible = false;

            PopulationRandomAmount_Group.Enabled = false;
            PopulationRandomAmount_Group.Visible = false;

            Buffs_ComboBox.SelectedIndex = Buffs_ComboBox.FindStringExact("No Buff");
            Debufs_ComboBox.SelectedIndex = Debufs_ComboBox.FindString("No Debuff");
            BuffDebuffDuration_TextBox.Text = "0";
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            #region Error Handlers
            //NOTE(Celso): This is our errors handlers.
            //If we don't have any text on the History Text Box we don't continue and we show the right error.
            if (String.IsNullOrEmpty(HistoryTextBox.Text))
            {
                MessageBox.Show(ErrorMessages.Message_Fail_NoHistoryText);
                return;
            }
            
            //If we don't have any Event Type / Advanced Event Type selected, we fail.
            if (String.IsNullOrEmpty(HistoryType_ComboBox.Text))
            {
                MessageBox.Show(ErrorMessages.Message_FailedNoEventType);
                return;
            }
            else if (String.IsNullOrEmpty(AdvancedType_ComboBox.Text))
            {
                MessageBox.Show(ErrorMessages.Message_FailedNoAdvancedType);
                return;
            }
            
            EventType eventType = EnumAnalyzer.EventAnalyzer(HistoryType_ComboBox.Text);
            
            string authorName = String.Empty;

            //If we don't have any Author, we fail.
            if (!String.IsNullOrEmpty(HistoryAutorTextBox.Text))
            {
                authorName = HistoryAutorTextBox.Text;
            }
            else
            {
                MessageBox.Show(ErrorMessages.Message_Fail_NoAuthor);
                return;
            }
            
            string historyName = String.Empty;

            //If we don't have any History Name, we fail.
            if (!String.IsNullOrEmpty(HistoryNameTextBox.Text))
            {
                historyName = HistoryNameTextBox.Text;
            }
            else
            {
                MessageBox.Show(ErrorMessages.Message_Fail_NoHistoryName);
                return;
            }

            RaceType _raceType;

            if (!String.IsNullOrEmpty(Race_ComboBox.Text))
            {
                _raceType = EnumAnalyzer.RaceTypeAnalyzer(Race_ComboBox.Text);
            }
            else
            {
                MessageBox.Show(ErrorMessages.Message_Fail_NoRaceDefinied);

                _raceType = RaceType.Commun;

                Race_ComboBox.SelectedItem = 0;
                return;
            }
            
            EventsBuffs buff = EventsBuffs.None;

            if (!String.IsNullOrEmpty(Buffs_ComboBox.Text))
            {
                buff = EnumAnalyzer.BuffTypeAnalizer(Buffs_ComboBox.Text);
            }

            EventsDebuffs debuff = EventsDebuffs.None;

            if (!String.IsNullOrEmpty(Debufs_ComboBox.Text))
            {
                debuff = EnumAnalyzer.DebuffTypeAnalyzer(Debufs_ComboBox.Text);
            }

            //NOTE(Celso): We're not showing error right now, but I want to show one 
            //if we have any Buff/Debuff to be assign and the Duration is equal 0.
            int effectDuration = 0;

            //If we have any Buff/Debuff selected we dont procced if we have no duration time.
            if (buff != EventsBuffs.None || debuff != EventsDebuffs.None)
            {
                if (!String.IsNullOrEmpty(BuffDebuffDuration_TextBox.Text))
                {
                    effectDuration = int.Parse(BuffDebuffDuration_TextBox.Text);
                }
                else
                {
                    MessageBox.Show(ErrorMessages.Message_Fail_BuffDebuffDuration);
                    return;
                }                
            }
            #endregion

            SaveFileDialog saveDialog = SaveDialog.CreateNewSaveDialog(@DirectoryController.HistoryPath);   //Create a new Save Dialog.
            string fileName;

            Object objectToWrite = new object();

            //NOTE(Celso): We need to pass the history name someway.
            switch (eventType)
            {
                case EventType.Resource:

                    #region Resource Event

                    EventResourceType resType = EnumAnalyzer.EventResourceAnalyzer(AdvancedType_ComboBox.Text);   //Analyze the type.

                    switch (ResourcesComboBox.Text)
                    {
                        #region Fixed Amount

                        case EventsTypeHelper.NoneAmount:
                            
                            //NOTE(Celso): Add a combo_box for the Race Type.
                            ResourceEvent resouceEvent = new ResourceEvent(HistoryTextBox.Text, eventType, _raceType, resType, EventAmountType.None);

                            //Set the autor, this could go on the constructor.
                            resouceEvent.Author = HistoryAutorTextBox.Text;
                            resouceEvent.Name = HistoryNameTextBox.Text;

                            resouceEvent.Buff = buff;
                            resouceEvent.Debuff = debuff;
                            resouceEvent.EffectDuration = effectDuration;

                            SaveDialog.ChangeSavePath(DirectoryController.HistoryResourcePath, saveDialog);

                            //The file name will be the HistoryType + ResourceType + Author
                            fileName = resouceEvent.HistoryType + "_" + resouceEvent.ResourceType + "_" + resouceEvent.Name + "_" + resouceEvent.Author;
                            fileName = fileName.Replace(" ", String.Empty);

                            saveDialog.FileName = fileName;      //Assign the generated name.

                            objectToWrite = resouceEvent;   //Assign the object we're writing.

                            break;


                        case EventsTypeHelper.FixedAmount:
                            float amount = 0;

                            if (!String.IsNullOrEmpty(ResourcesAmount.Text))    
                            {
                                amount = Single.Parse(ResourcesAmount.Text);  //Parse the amount from string to float.
                            }
                            
                            //NOTE(Celso): Add a combo_box for the Race Type.
                            ResourceEventFixed resEvent = new ResourceEventFixed(HistoryTextBox.Text, eventType, _raceType, resType, EventAmountType.Fixed, amount);

                            //Set the autor, this could go on the constructor.
                            resEvent.Author = HistoryAutorTextBox.Text;
                            resEvent.Name = HistoryNameTextBox.Text;

                            resEvent.Buff = buff;
                            resEvent.Debuff = debuff;
                            resEvent.EffectDuration = effectDuration;

                            SaveDialog.ChangeSavePath(DirectoryController.HistoryResourcePath, saveDialog);

                            //The file name will be the HistoryType + ResourceType + Author
                            fileName = resEvent.HistoryType + "_" + resEvent.ResourceType + "_" + resEvent.Name + "_" + resEvent.Author;                            
                            fileName = fileName.Replace(" ", String.Empty);

                            saveDialog.FileName = fileName;      //Assign the generated name.

                            objectToWrite = resEvent;   //Assign the object we're writing.
                            
                            break;

                        #endregion  

                        #region Random Amount

                        case EventsTypeHelper.RandomAmount:
                            //We should check for valids entrys here.
                            float minAmount = Single.Parse(MinAmountTextBox.Text);
                            float maxAmount = Single.Parse(MaxAmountTextBox.Text);

                            //NOTE(Celso): Add a combo_box for the Race Type.
                            HistoryEventRandom randomEvent = new HistoryEventRandom(HistoryTextBox.Text, eventType, _raceType, resType, EventAmountType.Random, minAmount, maxAmount);

                            randomEvent.Author = authorName;      //Set the author
                            randomEvent.Name = historyName;     //Set the history name

                            randomEvent.Buff = buff;
                            randomEvent.Debuff = debuff;
                            randomEvent.EffectDuration = effectDuration;

                            SaveDialog.ChangeSavePath(DirectoryController.HistoryResourcePath, saveDialog);

                            //The file name will be the HistoryType + ResourceType + Author
                            fileName = randomEvent.HistoryType + "_" + randomEvent.ResourceType + "_" + randomEvent.Name + "_" + randomEvent.Author;
                            fileName = fileName.Replace(" ", String.Empty);

                            saveDialog.FileName = fileName;      //Assign the generated name.

                            objectToWrite = randomEvent;

                            break;

                            #endregion
                    }

                    #endregion
                    break;
                    
                case EventType.Population:

                    #region Population Event

                    EventPopulationType popType = EnumAnalyzer.EventPopulationAnalyzer(AdvancedType_ComboBox.Text);
                    
                    switch (PopulationComboBox.Text)
                    {
                        case EventsTypeHelper.NoneAmount:

                            //NOTE(Celso): Add a combo_box for the Race Type.
                            PopulationEvent populationEvent = new PopulationEvent(HistoryTextBox.Text, eventType, _raceType, popType, EventAmountType.None);

                            //Set the autor, this could go on the constructor.
                            populationEvent.Author = HistoryAutorTextBox.Text;
                            populationEvent.Name = HistoryNameTextBox.Text;

                            populationEvent.Buff = buff;
                            populationEvent.Debuff = debuff;
                            populationEvent.EffectDuration = effectDuration;

                            SaveDialog.ChangeSavePath(DirectoryController.HistoryPopulationPath, saveDialog);

                            //The file name will be the HistoryType + ResourceType + Author
                            fileName = populationEvent.HistoryType + "_" + populationEvent.PopulationType + "_" + populationEvent.Name + "_" + populationEvent.Author;
                            fileName = fileName.Replace(" ", String.Empty);

                            saveDialog.FileName = fileName;      //Assign the generated name.

                            objectToWrite = populationEvent;   //Assign the object we're writing.

                            break;


                        #region Fixed Amount

                        case EventsTypeHelper.FixedAmount:
                            int popAmount = int.Parse(PopulationFixedAmount_AmountTextBox.Text);

                            //NOTE(Celso): Add a combo_box for the Race Type.
                            PopulationEventFixed popEvent = new PopulationEventFixed(HistoryTextBox.Text, eventType, _raceType, popType, EventAmountType.Fixed, popAmount);
                            popEvent.Author = HistoryAutorTextBox.Text;
                            popEvent.Name = historyName;

                            popEvent.Buff = buff;
                            popEvent.Debuff = debuff;
                            popEvent.EffectDuration = effectDuration;

                            SaveDialog.ChangeSavePath(DirectoryController.HistoryPopulationPath, saveDialog);

                            //The file name will be the HistoryType + ResourceType + Author
                            fileName = popEvent.HistoryType + "_" + popEvent.PopulationType + "_" + popEvent.Name + "_" + popEvent.Author;
                            fileName = fileName.Replace(" ", String.Empty);

                            saveDialog.FileName = fileName;

                            objectToWrite = popEvent;

                            break;

                        #endregion

                        #region Random Amount

                        case EventsTypeHelper.RandomAmount:
                            int minAmount = int.Parse(PopulationRandomAmount_MinAmount_TextBox.Text);
                            int maxAmount = int.Parse(PopulationRandomAmount_MaxAmount_TextBox.Text);

                            //NOTE(Celso): Add a combo_box for the Race Type.
                            PopulationEventRandom randomPop = new PopulationEventRandom(HistoryTextBox.Text, eventType, _raceType, popType, EventAmountType.Random, minAmount, maxAmount);
                            randomPop.Author = HistoryAutorTextBox.Text;
                            randomPop.Name = historyName;

                            randomPop.Buff = buff;
                            randomPop.Debuff = debuff;
                            randomPop.EffectDuration = effectDuration;

                            SaveDialog.ChangeSavePath(DirectoryController.HistoryPopulationPath, saveDialog);

                            fileName = randomPop.HistoryType + "_" + randomPop.PopulationType + "_" + randomPop.Name + "_" + randomPop.Author;
                            fileName = fileName.Replace(" ", String.Empty);

                            saveDialog.FileName = fileName;

                            objectToWrite = randomPop;

                            break;

                            #endregion
                    }
                    
                    #endregion
                    break;
            }

            if (SaveDialog.SaveNewObject(objectToWrite, saveDialog) == DialogResult.OK)
            {
                MessageBox.Show(ErrorMessages.Message_Success_GenerateResourcesData);                
            }
            else
            {
                MessageBox.Show(ErrorMessages.Message_Fail_CantSave);
            }
        }

        private void LoadJson_Button_Click(object sender, EventArgs e)
        {
            HistoryEvent temp = LoadFilesUtility.LoadFile();

            HistoryTextBox.Text = temp.HistoryText;

            HistoryType_ComboBox.SelectedIndex = HistoryType_ComboBox.FindString(temp.HistoryType.ToString());

            HistoryAutorTextBox.Text = temp.Author;
            HistoryNameTextBox.Text = temp.Name;

            Race_ComboBox.SelectedIndex = Race_ComboBox.FindString(temp.RaceType.ToString());

            BuffDebuffDuration_TextBox.Text = temp.EffectDuration.ToString();
            Buffs_ComboBox.SelectedIndex = Buffs_ComboBox.FindString(temp.Buff.ToString());
            Debufs_ComboBox.SelectedIndex = Debufs_ComboBox.FindString(temp.Debuff.ToString());
            
            ForceAdvancedComboBoxUpdate();

            switch (temp.HistoryType)
            {
                case EventType.Initial:
                    break;

                case EventType.Resource:                                                           
                    
                    ResourceEvent tempResource = temp as ResourceEvent;

                    AdvancedType_ComboBox.SelectedIndex =
                        AdvancedType_ComboBox.FindStringExact(tempResource.ResourceType.ToString());

                    ResourcesComboBox.SelectedIndex = ResourcesComboBox.FindString(tempResource.AmountType.ToString());

                    switch (tempResource.AmountType)
                    {
                        case EventAmountType.Fixed:

                            ResourceEventFixed newTemp = tempResource as ResourceEventFixed;
                            ResourcesAmount.Text = newTemp.Amount.ToString();

                            break;

                        case EventAmountType.Random:

                            HistoryEventRandom newTempRandom = tempResource as HistoryEventRandom;
                            MinAmountTextBox.Text = newTempRandom.MinimunAmount.ToString();
                            MaxAmountTextBox.Text = newTempRandom.MaximunAmount.ToString();

                            break;
                    }

                    break;

                case EventType.Population:

                    PopulationEvent tempPopulation = temp as PopulationEvent;
                    
                    AdvancedType_ComboBox.SelectedIndex =
                                AdvancedType_ComboBox.FindStringExact(tempPopulation.PopulationType.ToString());

                    PopulationComboBox.SelectedIndex =
                                PopulationComboBox.FindString(tempPopulation.AmountType.ToString());

                    switch (tempPopulation.AmountType)
                    {
                        case EventAmountType.Fixed:
                            
                            PopulationEventFixed newTemp = tempPopulation as PopulationEventFixed;
                            PopulationFixedAmount_AmountTextBox.Text = newTemp.Amount.ToString();

                            break;

                        case EventAmountType.Random:

                            PopulationEventRandom newTempRandom  = tempPopulation as PopulationEventRandom;
                            MinAmountTextBox.Text = newTempRandom.MinimunAmount.ToString();
                            MaxAmountTextBox.Text = newTempRandom.MaximunAmount.ToString();

                            break;
                    }
                    break;

                case EventType.Lore:
                    break;

                case EventType.Gods:
                    break;

                case EventType.Commun:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ForceAdvancedComboBoxUpdate()
        {
            if (HistoryType_ComboBox.Text == EventsTypeHelper.ResourceType)
            {
                AdvancedType_ComboBox.Items.Clear(); //Clear the List<> and add the items we need.

                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.IncreaseWater);
                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.DecreaseWater);
                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.IncreaseFood);
                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.DecreaseFood);
                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.IncreaseWood);
                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.DecreaseWood);

                ResourceGroup.Enabled = true;
                ResourceGroup.Visible = true;

                PopulationGroup.Enabled = false;
                PopulationGroup.Visible = false;
            }
            else if (HistoryType_ComboBox.Text == EventsTypeHelper.PopulationType)
            {
                AdvancedType_ComboBox.Items.Clear(); //Clear the List<> and add the items we need.

                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.PopulationArriving);
                AdvancedType_ComboBox.Items.Add(EventsTypeHelper.PopulationLeaving);

                ResourceGroup.Enabled = false;
                ResourceGroup.Visible = false;

                PopulationGroup.Enabled = true;
                PopulationGroup.Visible = true;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ForceAdvancedComboBoxUpdate();
        }

        private void ForceResoucesComboBoxUpdate()
        {
            switch (ResourcesComboBox.Text)
            {
                case EventsTypeHelper.FixedAmount:
                    RandomAmountGroup.Enabled = false;
                    RandomAmountGroup.Visible = false;

                    FixedAmountGroup.Enabled = true;
                    FixedAmountGroup.Visible = true;
                    break;

                case EventsTypeHelper.RandomAmount:


                    RandomAmountGroup.Enabled = true;
                    RandomAmountGroup.Visible = true;
                    break;

                default:
                    FixedAmountGroup.Enabled = false;
                    FixedAmountGroup.Visible = false;

                    RandomAmountGroup.Enabled = false;
                    RandomAmountGroup.Visible = false;
                    break;
            }
        }

        private void ResourcesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ForceResoucesComboBoxUpdate();
        }

        private void ForcePopulationComboBoxUpdate()
        {
            switch (PopulationComboBox.Text)
            {
                case EventsTypeHelper.FixedAmount:
                    PopulationFixedAmount_Group.Enabled = true;
                    PopulationFixedAmount_Group.Visible = true;

                    PopulationRandomAmount_Group.Enabled = false;
                    PopulationRandomAmount_Group.Visible = false;

                    break;

                case EventsTypeHelper.RandomAmount:
                    PopulationFixedAmount_Group.Enabled = false;
                    PopulationFixedAmount_Group.Visible = false;

                    PopulationRandomAmount_Group.Enabled = true;
                    PopulationRandomAmount_Group.Visible = true;
                    break;

                default:
                    PopulationFixedAmount_Group.Enabled = false;
                    PopulationFixedAmount_Group.Visible = false;

                    PopulationRandomAmount_Group.Enabled = false;
                    PopulationRandomAmount_Group.Visible = false;
                    break;
            }
        }

        private void PopComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ForcePopulationComboBoxUpdate();
        }
    }
}