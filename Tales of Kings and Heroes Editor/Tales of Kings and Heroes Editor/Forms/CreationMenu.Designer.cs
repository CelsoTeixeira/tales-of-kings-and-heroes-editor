namespace Tales_of_Kings_and_Heroes_Editor
{
    partial class CreationMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HistoryTextBox = new System.Windows.Forms.TextBox();
            this.GenerateJson = new System.Windows.Forms.Button();
            this.HistoryType_ComboBox = new System.Windows.Forms.ComboBox();
            this.HistoryType_Label = new System.Windows.Forms.Label();
            this.HistoryBox_Label = new System.Windows.Forms.Label();
            this.AdvancedType_ComboBox = new System.Windows.Forms.ComboBox();
            this.HistoryAutorTextBox = new System.Windows.Forms.TextBox();
            this.HistoryAutor_Label = new System.Windows.Forms.Label();
            this.ResourcesAmount = new System.Windows.Forms.TextBox();
            this.ResourcesAmountLabel = new System.Windows.Forms.Label();
            this.MinAmountLabel = new System.Windows.Forms.Label();
            this.MaxAmountLabel = new System.Windows.Forms.Label();
            this.MinAmountTextBox = new System.Windows.Forms.TextBox();
            this.MaxAmountTextBox = new System.Windows.Forms.TextBox();
            this.ResourcesComboBox = new System.Windows.Forms.ComboBox();
            this.HistoryNameTextBox = new System.Windows.Forms.TextBox();
            this.HistoryName_Label = new System.Windows.Forms.Label();
            this.ResourceGroup = new System.Windows.Forms.GroupBox();
            this.RandomAmountGroup = new System.Windows.Forms.GroupBox();
            this.FixedAmountGroup = new System.Windows.Forms.GroupBox();
            this.PopulationGroup = new System.Windows.Forms.GroupBox();
            this.PopulationRandomAmount_Group = new System.Windows.Forms.GroupBox();
            this.PopulationRandomAmount_MaxAmount_TextBox = new System.Windows.Forms.TextBox();
            this.PopulationRandomAmount_MinAmount_TextBox = new System.Windows.Forms.TextBox();
            this.PopulationRandomMaxAmount_Label = new System.Windows.Forms.Label();
            this.RandomAmountMinAmount_Label = new System.Windows.Forms.Label();
            this.PopulationFixedAmount_Group = new System.Windows.Forms.GroupBox();
            this.PopulationFixedAmount_AmountTextBox = new System.Windows.Forms.TextBox();
            this.PopulationFixedAmountAmount_Label = new System.Windows.Forms.Label();
            this.PopulationComboBox = new System.Windows.Forms.ComboBox();
            this.AdvancedType_Label = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.BuffDebuff_Group = new System.Windows.Forms.GroupBox();
            this.BuffDebuffDuration_TextBox = new System.Windows.Forms.TextBox();
            this.BuffDebuffDuration_Label = new System.Windows.Forms.Label();
            this.Debufs_ComboBox = new System.Windows.Forms.ComboBox();
            this.Buffs_ComboBox = new System.Windows.Forms.ComboBox();
            this.Race_ComboBox = new System.Windows.Forms.ComboBox();
            this.Race_Label = new System.Windows.Forms.Label();
            this.LoadJson_Button = new System.Windows.Forms.Button();
            this.ResourceGroup.SuspendLayout();
            this.RandomAmountGroup.SuspendLayout();
            this.FixedAmountGroup.SuspendLayout();
            this.PopulationGroup.SuspendLayout();
            this.PopulationRandomAmount_Group.SuspendLayout();
            this.PopulationFixedAmount_Group.SuspendLayout();
            this.BuffDebuff_Group.SuspendLayout();
            this.SuspendLayout();
            // 
            // HistoryTextBox
            // 
            this.HistoryTextBox.Location = new System.Drawing.Point(12, 23);
            this.HistoryTextBox.Multiline = true;
            this.HistoryTextBox.Name = "HistoryTextBox";
            this.HistoryTextBox.Size = new System.Drawing.Size(264, 362);
            this.HistoryTextBox.TabIndex = 0;
            // 
            // GenerateJson
            // 
            this.GenerateJson.Location = new System.Drawing.Point(494, 302);
            this.GenerateJson.Name = "GenerateJson";
            this.GenerateJson.Size = new System.Drawing.Size(68, 43);
            this.GenerateJson.TabIndex = 2;
            this.GenerateJson.Text = "Gerar JSON";
            this.GenerateJson.UseVisualStyleBackColor = true;
            this.GenerateJson.Click += new System.EventHandler(this.button1_Click);
            // 
            // HistoryType_ComboBox
            // 
            this.HistoryType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HistoryType_ComboBox.FormattingEnabled = true;
            this.HistoryType_ComboBox.Items.AddRange(new object[] {
            "Resource",
            "Population"});
            this.HistoryType_ComboBox.Location = new System.Drawing.Point(288, 39);
            this.HistoryType_ComboBox.Name = "HistoryType_ComboBox";
            this.HistoryType_ComboBox.Size = new System.Drawing.Size(129, 21);
            this.HistoryType_ComboBox.TabIndex = 5;
            this.HistoryType_ComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // HistoryType_Label
            // 
            this.HistoryType_Label.AutoSize = true;
            this.HistoryType_Label.Location = new System.Drawing.Point(292, 23);
            this.HistoryType_Label.Name = "HistoryType_Label";
            this.HistoryType_Label.Size = new System.Drawing.Size(66, 13);
            this.HistoryType_Label.TabIndex = 6;
            this.HistoryType_Label.Text = "History Type";
            // 
            // HistoryBox_Label
            // 
            this.HistoryBox_Label.AutoSize = true;
            this.HistoryBox_Label.Location = new System.Drawing.Point(12, 7);
            this.HistoryBox_Label.Name = "HistoryBox_Label";
            this.HistoryBox_Label.Size = new System.Drawing.Size(59, 13);
            this.HistoryBox_Label.TabIndex = 7;
            this.HistoryBox_Label.Text = "History box";
            // 
            // AdvancedType_ComboBox
            // 
            this.AdvancedType_ComboBox.FormattingEnabled = true;
            this.AdvancedType_ComboBox.Location = new System.Drawing.Point(433, 39);
            this.AdvancedType_ComboBox.Name = "AdvancedType_ComboBox";
            this.AdvancedType_ComboBox.Size = new System.Drawing.Size(129, 21);
            this.AdvancedType_ComboBox.TabIndex = 8;
            // 
            // HistoryAutorTextBox
            // 
            this.HistoryAutorTextBox.Location = new System.Drawing.Point(363, 366);
            this.HistoryAutorTextBox.Name = "HistoryAutorTextBox";
            this.HistoryAutorTextBox.Size = new System.Drawing.Size(120, 20);
            this.HistoryAutorTextBox.TabIndex = 9;
            // 
            // HistoryAutor_Label
            // 
            this.HistoryAutor_Label.AutoSize = true;
            this.HistoryAutor_Label.Location = new System.Drawing.Point(282, 369);
            this.HistoryAutor_Label.Name = "HistoryAutor_Label";
            this.HistoryAutor_Label.Size = new System.Drawing.Size(76, 13);
            this.HistoryAutor_Label.TabIndex = 10;
            this.HistoryAutor_Label.Text = "History Author:";
            // 
            // ResourcesAmount
            // 
            this.ResourcesAmount.Location = new System.Drawing.Point(88, 23);
            this.ResourcesAmount.Name = "ResourcesAmount";
            this.ResourcesAmount.Size = new System.Drawing.Size(69, 20);
            this.ResourcesAmount.TabIndex = 12;
            // 
            // ResourcesAmountLabel
            // 
            this.ResourcesAmountLabel.AutoSize = true;
            this.ResourcesAmountLabel.Location = new System.Drawing.Point(6, 26);
            this.ResourcesAmountLabel.Name = "ResourcesAmountLabel";
            this.ResourcesAmountLabel.Size = new System.Drawing.Size(46, 13);
            this.ResourcesAmountLabel.TabIndex = 13;
            this.ResourcesAmountLabel.Text = "Amount:";
            // 
            // MinAmountLabel
            // 
            this.MinAmountLabel.AutoSize = true;
            this.MinAmountLabel.Location = new System.Drawing.Point(6, 26);
            this.MinAmountLabel.Name = "MinAmountLabel";
            this.MinAmountLabel.Size = new System.Drawing.Size(66, 13);
            this.MinAmountLabel.TabIndex = 15;
            this.MinAmountLabel.Text = "Min Amount:";
            // 
            // MaxAmountLabel
            // 
            this.MaxAmountLabel.AutoSize = true;
            this.MaxAmountLabel.Location = new System.Drawing.Point(6, 52);
            this.MaxAmountLabel.Name = "MaxAmountLabel";
            this.MaxAmountLabel.Size = new System.Drawing.Size(69, 13);
            this.MaxAmountLabel.TabIndex = 16;
            this.MaxAmountLabel.Text = "Max Amount:";
            // 
            // MinAmountTextBox
            // 
            this.MinAmountTextBox.Location = new System.Drawing.Point(88, 23);
            this.MinAmountTextBox.Name = "MinAmountTextBox";
            this.MinAmountTextBox.Size = new System.Drawing.Size(69, 20);
            this.MinAmountTextBox.TabIndex = 17;
            // 
            // MaxAmountTextBox
            // 
            this.MaxAmountTextBox.Location = new System.Drawing.Point(88, 49);
            this.MaxAmountTextBox.Name = "MaxAmountTextBox";
            this.MaxAmountTextBox.Size = new System.Drawing.Size(69, 20);
            this.MaxAmountTextBox.TabIndex = 18;
            // 
            // ResourcesComboBox
            // 
            this.ResourcesComboBox.FormattingEnabled = true;
            this.ResourcesComboBox.Items.AddRange(new object[] {
            "FixedAmount",
            "RandomAmount",
            "None"});
            this.ResourcesComboBox.Location = new System.Drawing.Point(6, 19);
            this.ResourcesComboBox.Name = "ResourcesComboBox";
            this.ResourcesComboBox.Size = new System.Drawing.Size(121, 21);
            this.ResourcesComboBox.TabIndex = 19;
            this.ResourcesComboBox.SelectedIndexChanged += new System.EventHandler(this.ResourcesComboBox_SelectedIndexChanged);
            // 
            // HistoryNameTextBox
            // 
            this.HistoryNameTextBox.Location = new System.Drawing.Point(363, 340);
            this.HistoryNameTextBox.Name = "HistoryNameTextBox";
            this.HistoryNameTextBox.Size = new System.Drawing.Size(120, 20);
            this.HistoryNameTextBox.TabIndex = 20;
            // 
            // HistoryName_Label
            // 
            this.HistoryName_Label.AutoSize = true;
            this.HistoryName_Label.Location = new System.Drawing.Point(287, 343);
            this.HistoryName_Label.Name = "HistoryName_Label";
            this.HistoryName_Label.Size = new System.Drawing.Size(73, 13);
            this.HistoryName_Label.TabIndex = 21;
            this.HistoryName_Label.Text = "History Name:";
            // 
            // ResourceGroup
            // 
            this.ResourceGroup.Controls.Add(this.RandomAmountGroup);
            this.ResourceGroup.Controls.Add(this.ResourcesComboBox);
            this.ResourceGroup.Controls.Add(this.FixedAmountGroup);
            this.ResourceGroup.Location = new System.Drawing.Point(288, 158);
            this.ResourceGroup.Name = "ResourceGroup";
            this.ResourceGroup.Size = new System.Drawing.Size(269, 138);
            this.ResourceGroup.TabIndex = 22;
            this.ResourceGroup.TabStop = false;
            this.ResourceGroup.Text = "Resources";
            // 
            // RandomAmountGroup
            // 
            this.RandomAmountGroup.Controls.Add(this.MinAmountLabel);
            this.RandomAmountGroup.Controls.Add(this.MinAmountTextBox);
            this.RandomAmountGroup.Controls.Add(this.MaxAmountLabel);
            this.RandomAmountGroup.Controls.Add(this.MaxAmountTextBox);
            this.RandomAmountGroup.Location = new System.Drawing.Point(7, 46);
            this.RandomAmountGroup.Name = "RandomAmountGroup";
            this.RandomAmountGroup.Size = new System.Drawing.Size(160, 78);
            this.RandomAmountGroup.TabIndex = 24;
            this.RandomAmountGroup.TabStop = false;
            this.RandomAmountGroup.Text = "Random Amount";
            // 
            // FixedAmountGroup
            // 
            this.FixedAmountGroup.Controls.Add(this.ResourcesAmountLabel);
            this.FixedAmountGroup.Controls.Add(this.ResourcesAmount);
            this.FixedAmountGroup.Location = new System.Drawing.Point(6, 46);
            this.FixedAmountGroup.Name = "FixedAmountGroup";
            this.FixedAmountGroup.Size = new System.Drawing.Size(163, 73);
            this.FixedAmountGroup.TabIndex = 23;
            this.FixedAmountGroup.TabStop = false;
            this.FixedAmountGroup.Text = "Fixed Amount";
            // 
            // PopulationGroup
            // 
            this.PopulationGroup.Controls.Add(this.PopulationRandomAmount_Group);
            this.PopulationGroup.Controls.Add(this.PopulationFixedAmount_Group);
            this.PopulationGroup.Controls.Add(this.PopulationComboBox);
            this.PopulationGroup.Location = new System.Drawing.Point(288, 158);
            this.PopulationGroup.Name = "PopulationGroup";
            this.PopulationGroup.Size = new System.Drawing.Size(269, 138);
            this.PopulationGroup.TabIndex = 23;
            this.PopulationGroup.TabStop = false;
            this.PopulationGroup.Text = "Population";
            // 
            // PopulationRandomAmount_Group
            // 
            this.PopulationRandomAmount_Group.Controls.Add(this.PopulationRandomAmount_MaxAmount_TextBox);
            this.PopulationRandomAmount_Group.Controls.Add(this.PopulationRandomAmount_MinAmount_TextBox);
            this.PopulationRandomAmount_Group.Controls.Add(this.PopulationRandomMaxAmount_Label);
            this.PopulationRandomAmount_Group.Controls.Add(this.RandomAmountMinAmount_Label);
            this.PopulationRandomAmount_Group.Location = new System.Drawing.Point(7, 46);
            this.PopulationRandomAmount_Group.Name = "PopulationRandomAmount_Group";
            this.PopulationRandomAmount_Group.Size = new System.Drawing.Size(234, 85);
            this.PopulationRandomAmount_Group.TabIndex = 24;
            this.PopulationRandomAmount_Group.TabStop = false;
            this.PopulationRandomAmount_Group.Text = "Random Amount";
            // 
            // PopulationRandomAmount_MaxAmount_TextBox
            // 
            this.PopulationRandomAmount_MaxAmount_TextBox.Location = new System.Drawing.Point(82, 44);
            this.PopulationRandomAmount_MaxAmount_TextBox.Name = "PopulationRandomAmount_MaxAmount_TextBox";
            this.PopulationRandomAmount_MaxAmount_TextBox.Size = new System.Drawing.Size(88, 20);
            this.PopulationRandomAmount_MaxAmount_TextBox.TabIndex = 3;
            // 
            // PopulationRandomAmount_MinAmount_TextBox
            // 
            this.PopulationRandomAmount_MinAmount_TextBox.Location = new System.Drawing.Point(82, 18);
            this.PopulationRandomAmount_MinAmount_TextBox.Name = "PopulationRandomAmount_MinAmount_TextBox";
            this.PopulationRandomAmount_MinAmount_TextBox.Size = new System.Drawing.Size(88, 20);
            this.PopulationRandomAmount_MinAmount_TextBox.TabIndex = 2;
            // 
            // PopulationRandomMaxAmount_Label
            // 
            this.PopulationRandomMaxAmount_Label.AutoSize = true;
            this.PopulationRandomMaxAmount_Label.Location = new System.Drawing.Point(6, 53);
            this.PopulationRandomMaxAmount_Label.Name = "PopulationRandomMaxAmount_Label";
            this.PopulationRandomMaxAmount_Label.Size = new System.Drawing.Size(69, 13);
            this.PopulationRandomMaxAmount_Label.TabIndex = 1;
            this.PopulationRandomMaxAmount_Label.Text = "Max Amount:";
            // 
            // RandomAmountMinAmount_Label
            // 
            this.RandomAmountMinAmount_Label.AutoSize = true;
            this.RandomAmountMinAmount_Label.Location = new System.Drawing.Point(6, 22);
            this.RandomAmountMinAmount_Label.Name = "RandomAmountMinAmount_Label";
            this.RandomAmountMinAmount_Label.Size = new System.Drawing.Size(66, 13);
            this.RandomAmountMinAmount_Label.TabIndex = 0;
            this.RandomAmountMinAmount_Label.Text = "Min Amount:";
            // 
            // PopulationFixedAmount_Group
            // 
            this.PopulationFixedAmount_Group.Controls.Add(this.PopulationFixedAmount_AmountTextBox);
            this.PopulationFixedAmount_Group.Controls.Add(this.PopulationFixedAmountAmount_Label);
            this.PopulationFixedAmount_Group.Location = new System.Drawing.Point(6, 46);
            this.PopulationFixedAmount_Group.Name = "PopulationFixedAmount_Group";
            this.PopulationFixedAmount_Group.Size = new System.Drawing.Size(236, 86);
            this.PopulationFixedAmount_Group.TabIndex = 2;
            this.PopulationFixedAmount_Group.TabStop = false;
            this.PopulationFixedAmount_Group.Text = "Fixed Amount";
            // 
            // PopulationFixedAmount_AmountTextBox
            // 
            this.PopulationFixedAmount_AmountTextBox.Location = new System.Drawing.Point(58, 20);
            this.PopulationFixedAmount_AmountTextBox.Name = "PopulationFixedAmount_AmountTextBox";
            this.PopulationFixedAmount_AmountTextBox.Size = new System.Drawing.Size(75, 20);
            this.PopulationFixedAmount_AmountTextBox.TabIndex = 1;
            // 
            // PopulationFixedAmountAmount_Label
            // 
            this.PopulationFixedAmountAmount_Label.AutoSize = true;
            this.PopulationFixedAmountAmount_Label.Location = new System.Drawing.Point(7, 23);
            this.PopulationFixedAmountAmount_Label.Name = "PopulationFixedAmountAmount_Label";
            this.PopulationFixedAmountAmount_Label.Size = new System.Drawing.Size(46, 13);
            this.PopulationFixedAmountAmount_Label.TabIndex = 0;
            this.PopulationFixedAmountAmount_Label.Text = "Amount:";
            // 
            // PopulationComboBox
            // 
            this.PopulationComboBox.FormattingEnabled = true;
            this.PopulationComboBox.Items.AddRange(new object[] {
            "FixedAmount",
            "RandomAmount",
            "None"});
            this.PopulationComboBox.Location = new System.Drawing.Point(6, 19);
            this.PopulationComboBox.Name = "PopulationComboBox";
            this.PopulationComboBox.Size = new System.Drawing.Size(105, 21);
            this.PopulationComboBox.TabIndex = 0;
            this.PopulationComboBox.SelectedIndexChanged += new System.EventHandler(this.PopComboBox_SelectedIndexChanged);
            // 
            // AdvancedType_Label
            // 
            this.AdvancedType_Label.AutoSize = true;
            this.AdvancedType_Label.Location = new System.Drawing.Point(443, 23);
            this.AdvancedType_Label.Name = "AdvancedType_Label";
            this.AdvancedType_Label.Size = new System.Drawing.Size(83, 13);
            this.AdvancedType_Label.TabIndex = 24;
            this.AdvancedType_Label.Text = "Advanced Type";
            // 
            // BuffDebuff_Group
            // 
            this.BuffDebuff_Group.Controls.Add(this.BuffDebuffDuration_TextBox);
            this.BuffDebuff_Group.Controls.Add(this.BuffDebuffDuration_Label);
            this.BuffDebuff_Group.Controls.Add(this.Debufs_ComboBox);
            this.BuffDebuff_Group.Controls.Add(this.Buffs_ComboBox);
            this.BuffDebuff_Group.Location = new System.Drawing.Point(288, 66);
            this.BuffDebuff_Group.Name = "BuffDebuff_Group";
            this.BuffDebuff_Group.Size = new System.Drawing.Size(269, 86);
            this.BuffDebuff_Group.TabIndex = 25;
            this.BuffDebuff_Group.TabStop = false;
            this.BuffDebuff_Group.Text = "Buffs / Debuffs";
            // 
            // BuffDebuffDuration_TextBox
            // 
            this.BuffDebuffDuration_TextBox.Location = new System.Drawing.Point(55, 56);
            this.BuffDebuffDuration_TextBox.Name = "BuffDebuffDuration_TextBox";
            this.BuffDebuffDuration_TextBox.Size = new System.Drawing.Size(69, 20);
            this.BuffDebuffDuration_TextBox.TabIndex = 3;
            // 
            // BuffDebuffDuration_Label
            // 
            this.BuffDebuffDuration_Label.AutoSize = true;
            this.BuffDebuffDuration_Label.Location = new System.Drawing.Point(6, 59);
            this.BuffDebuffDuration_Label.Name = "BuffDebuffDuration_Label";
            this.BuffDebuffDuration_Label.Size = new System.Drawing.Size(50, 13);
            this.BuffDebuffDuration_Label.TabIndex = 2;
            this.BuffDebuffDuration_Label.Text = "Duration:";
            // 
            // Debufs_ComboBox
            // 
            this.Debufs_ComboBox.FormattingEnabled = true;
            this.Debufs_ComboBox.Items.AddRange(new object[] {
            "No Debuff",
            "Debuff1",
            "Debuff2",
            "Debuff3"});
            this.Debufs_ComboBox.Location = new System.Drawing.Point(137, 19);
            this.Debufs_ComboBox.Name = "Debufs_ComboBox";
            this.Debufs_ComboBox.Size = new System.Drawing.Size(115, 21);
            this.Debufs_ComboBox.TabIndex = 1;
            // 
            // Buffs_ComboBox
            // 
            this.Buffs_ComboBox.FormattingEnabled = true;
            this.Buffs_ComboBox.Items.AddRange(new object[] {
            "No Buff",
            "Buff1",
            "Buff2",
            "BUff3"});
            this.Buffs_ComboBox.Location = new System.Drawing.Point(16, 19);
            this.Buffs_ComboBox.Name = "Buffs_ComboBox";
            this.Buffs_ComboBox.Size = new System.Drawing.Size(115, 21);
            this.Buffs_ComboBox.TabIndex = 0;
            // 
            // Race_ComboBox
            // 
            this.Race_ComboBox.FormattingEnabled = true;
            this.Race_ComboBox.Items.AddRange(new object[] {
            "Commun",
            "Race 1",
            "Race 2",
            "Race 3"});
            this.Race_ComboBox.Location = new System.Drawing.Point(363, 313);
            this.Race_ComboBox.Name = "Race_ComboBox";
            this.Race_ComboBox.Size = new System.Drawing.Size(120, 21);
            this.Race_ComboBox.TabIndex = 26;
            // 
            // Race_Label
            // 
            this.Race_Label.AutoSize = true;
            this.Race_Label.Location = new System.Drawing.Point(324, 316);
            this.Race_Label.Name = "Race_Label";
            this.Race_Label.Size = new System.Drawing.Size(36, 13);
            this.Race_Label.TabIndex = 27;
            this.Race_Label.Text = "Race:";
            // 
            // LoadJson_Button
            // 
            this.LoadJson_Button.Location = new System.Drawing.Point(494, 351);
            this.LoadJson_Button.Name = "LoadJson_Button";
            this.LoadJson_Button.Size = new System.Drawing.Size(68, 43);
            this.LoadJson_Button.TabIndex = 28;
            this.LoadJson_Button.Text = "Load JSON";
            this.LoadJson_Button.UseVisualStyleBackColor = true;
            this.LoadJson_Button.Click += new System.EventHandler(this.LoadJson_Button_Click);
            // 
            // CreationMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 397);
            this.Controls.Add(this.LoadJson_Button);
            this.Controls.Add(this.Race_Label);
            this.Controls.Add(this.Race_ComboBox);
            this.Controls.Add(this.BuffDebuff_Group);
            this.Controls.Add(this.AdvancedType_Label);
            this.Controls.Add(this.HistoryName_Label);
            this.Controls.Add(this.HistoryNameTextBox);
            this.Controls.Add(this.HistoryAutor_Label);
            this.Controls.Add(this.HistoryAutorTextBox);
            this.Controls.Add(this.AdvancedType_ComboBox);
            this.Controls.Add(this.HistoryBox_Label);
            this.Controls.Add(this.HistoryType_Label);
            this.Controls.Add(this.HistoryType_ComboBox);
            this.Controls.Add(this.GenerateJson);
            this.Controls.Add(this.HistoryTextBox);
            this.Controls.Add(this.PopulationGroup);
            this.Controls.Add(this.ResourceGroup);
            this.Name = "CreationMenu";
            this.Text = "Creation Tab";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResourceGroup.ResumeLayout(false);
            this.RandomAmountGroup.ResumeLayout(false);
            this.RandomAmountGroup.PerformLayout();
            this.FixedAmountGroup.ResumeLayout(false);
            this.FixedAmountGroup.PerformLayout();
            this.PopulationGroup.ResumeLayout(false);
            this.PopulationRandomAmount_Group.ResumeLayout(false);
            this.PopulationRandomAmount_Group.PerformLayout();
            this.PopulationFixedAmount_Group.ResumeLayout(false);
            this.PopulationFixedAmount_Group.PerformLayout();
            this.BuffDebuff_Group.ResumeLayout(false);
            this.BuffDebuff_Group.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox HistoryTextBox;
        private System.Windows.Forms.Button GenerateJson;
        private System.Windows.Forms.ComboBox HistoryType_ComboBox;
        private System.Windows.Forms.Label HistoryType_Label;
        private System.Windows.Forms.Label HistoryBox_Label;
        private System.Windows.Forms.ComboBox AdvancedType_ComboBox;
        private System.Windows.Forms.TextBox HistoryAutorTextBox;
        private System.Windows.Forms.Label HistoryAutor_Label;
        private System.Windows.Forms.TextBox ResourcesAmount;
        private System.Windows.Forms.Label ResourcesAmountLabel;
        private System.Windows.Forms.Label MinAmountLabel;
        private System.Windows.Forms.Label MaxAmountLabel;
        private System.Windows.Forms.TextBox MinAmountTextBox;
        private System.Windows.Forms.TextBox MaxAmountTextBox;
        private System.Windows.Forms.ComboBox ResourcesComboBox;
        private System.Windows.Forms.TextBox HistoryNameTextBox;
        private System.Windows.Forms.Label HistoryName_Label;
        private System.Windows.Forms.GroupBox ResourceGroup;
        private System.Windows.Forms.GroupBox FixedAmountGroup;
        private System.Windows.Forms.GroupBox RandomAmountGroup;
        private System.Windows.Forms.GroupBox PopulationGroup;
        private System.Windows.Forms.ComboBox PopulationComboBox;
        private System.Windows.Forms.GroupBox PopulationFixedAmount_Group;
        private System.Windows.Forms.TextBox PopulationFixedAmount_AmountTextBox;
        private System.Windows.Forms.Label PopulationFixedAmountAmount_Label;
        private System.Windows.Forms.GroupBox PopulationRandomAmount_Group;
        private System.Windows.Forms.TextBox PopulationRandomAmount_MaxAmount_TextBox;
        private System.Windows.Forms.TextBox PopulationRandomAmount_MinAmount_TextBox;
        private System.Windows.Forms.Label PopulationRandomMaxAmount_Label;
        private System.Windows.Forms.Label RandomAmountMinAmount_Label;
        private System.Windows.Forms.Label AdvancedType_Label;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox BuffDebuff_Group;
        private System.Windows.Forms.ComboBox Debufs_ComboBox;
        private System.Windows.Forms.ComboBox Buffs_ComboBox;
        private System.Windows.Forms.ComboBox Race_ComboBox;
        private System.Windows.Forms.Label Race_Label;
        private System.Windows.Forms.TextBox BuffDebuffDuration_TextBox;
        private System.Windows.Forms.Label BuffDebuffDuration_Label;
        private System.Windows.Forms.Button LoadJson_Button;
    }
}

