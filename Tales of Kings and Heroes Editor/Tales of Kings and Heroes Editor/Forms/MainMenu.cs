using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tales_of_Kings_and_Heroes_Editor.Forms
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tales_of_Kings_and_Heroes_Editor.CreationMenu f = new CreationMenu();
            f.Show();
        }

        private void Edit_Button_Click(object sender, EventArgs e)
        {
            Visualize f = new Visualize();
            f.Show();
        }
    }
}
