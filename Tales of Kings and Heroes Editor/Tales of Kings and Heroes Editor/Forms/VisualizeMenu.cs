using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tales_of_Kings_and_Heroes_Editor.Editor;

namespace Tales_of_Kings_and_Heroes_Editor.Forms
{
    public partial class Visualize : Form
    {
        public Visualize()
        {
            InitializeComponent();
        }

        private void Visualize_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            HistoryEvent temp = LoadFilesUtility.LoadFile();

            textBox1.Text = temp.HistoryText;
        }
    }
}
