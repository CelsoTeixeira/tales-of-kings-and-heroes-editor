using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tales_of_Kings_and_Heroes_Editor.Class;
using MainMenu = Tales_of_Kings_and_Heroes_Editor.Forms.MainMenu;

namespace Tales_of_Kings_and_Heroes_Editor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //We're creating the folder on the default Path and storing it on the variable.
            DirectoryController.SetupDefaultPath();
            
            Application.Run(new MainMenu());            
        }        
    }
}
